# README #

Turns your laptop into a remote controlled audio player. Yeah I know you can get remote controlled speakers in the market, but the real fun lies in making one for yourself.

### Setup ###

* Clone the repo
* Watch the video to get a better understanding
* Just run the code on an Arduino (It already has an inbuilt IR receiver)


### Contribution guidelines ###

* Writing tests
* Playlists
* Code review